# Sogo LDAP installation on Ubuntu 20.04.1 LTS

## Installation steps
### Disable ssh password login
```shell
sudo nano /etc/ssh/sshd_config
```
Change UsePAM from 'yes' to 'no'
### Reload sshd to apply new configuration
```shell
sudo service sshd reload
```
### Change the hostname
```shell
sudo nano /etc/hostname
```
Put new hostname, then reboot

### Configure the timezone
```shell
sudo dpkg-reconfigure tzdata
```

Choose settings according to your current location

### System update
```shell
apt update
apt upgrade
```

### Reboot system


### Generate and set new password
```shell
openssl rand -base64 32
```

Use the generated string as the ldap password 

By default slapd use sample data to view your config use ``slapcat``

To configure it run ``dpkg-reconfigure slapd``

Select "no" when asked to omit OpenLDAP server configuration

Use the hostname as domain name

Choose the name of the oragnisation (main in our case)

Reenter same password

Choose to remove SLAPD databse en slapd is removed

Choose to remove old database



Create a infrasi-project.ldif file and paste this content into it: 
```
dn: ou=Users,dc=infrasi-project
objectClass: organizationalUnit
objectClass: top
ou: Users

dn: ou=Groups,dc=infrasi-project
objectClass: organizationalUnit
objectClass: top
ou: Groups
```

Create new user class with the newly created file
```shell
sudo ldapadd -x -D cn=admin,dc=infrasi-project -W -f infrasi-project.ldif
```

```
dn: uid=sogo,ou=Users,dc=infrasi-project,dc=tld
objectClass: top
objectClass: inetOrgPerson
objectClass: person
objectClass: organizationalPerson
uid: sogo
cn: SOGo Administrator
mail: sogo@infrasi-project
sn: Administrator
givenName: SOGo
```

```shell
sudo ldapadd -x -D cn=admin,dc=infrasi-project -W -f sogo.ldif
sudo ldappasswd -h localhost -x -w PASSWORD -D cn=admin,dc=infrasi-project uid=sogo,ou=Users,dc=infrasi-project -s PASSWORD
```

### Install OwnCloud
```shell
wget -nv https://download.owncloud.org/download/repositories/production/Ubuntu_20.04/Release.key -O - | sudo apt-key add -
echo 'deb http://download.owncloud.org/download/repositories/production/Ubuntu_20.04/ /' | sudo tee -a /etc/apt/sources.list.d/owncloud.list
sudo apt update
sudo apt install owncloud-complete-files
```

```shell
FILE="/usr/local/bin/occ"
/bin/cat <<EOM >$FILE
#! /bin/bash
cd /var/www/owncloud
sudo -u www-data /usr/bin/php /var/www/owncloud/occ "\$@"
EOM

chmod +x /usr/local/bin/occ
```

### Install Apache2, MariaDB and Php dependencies
```shell
apt install -y \
apache2 \
libapache2-mod-php \
mariadb-server \
php-imagick php-common php-curl \
php-gd php-imap php-intl \
php-json php-mbstring php-mysql \
php-ssh2 php-xml php-zip \
php-apcu php-redis redis-server
```

Check if apache2 is well running by accessing your server ip address with your navigator


### Update owncloud part of apache2's configuration
```shell
FILE="/etc/apache2/sites-available/owncloud.conf"
/bin/cat <<EOM >$FILE
Alias /owncloud "/var/www/owncloud/"

<Directory /var/www/owncloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/owncloud
 SetEnv HTTP_HOME /var/www/owncloud
</Directory>
EOM

a2ensite owncloud.conf
service apache2 reload
```

#### Setup database
Generate new password for database
```shell
openssl rand -base64 32 --> generate password
```

Create new database 'owncloud'
```shell
mysql -u root -e "CREATE DATABASE IF NOT EXISTS owncloud; \
GRANT ALL PRIVILEGES ON owncloud.* \
  TO owncloud@localhost \
  IDENTIFIED BY 'PASSWORD'";
```

#### Enable apache module

a2enmod dir env headers mime rewrite setenvif
service apache2 reload

  Now use web interface to config owncloud

  Db login: owncloud
  Db password: as generated earlier
  Db name: owncloud
  Host: localhost

Go to the left menu burger click on market and search for ldap integration click on install

Then click on your use on the right side of the menu and click settings then User Authentification

If a red notice warns you about the fact than the php ldap module is not installed, run the following lines and then reload the page
```shell
sudo apt install php-ldap
phpenmod ldap
service apache2 reload
```





### Install iRedMail

Check the hostname
```shell
hostname -f
```

Change the hostname to the FQDN shortname
```shell
sudo hostnamectl set-hostname <newName>
```

Now, the hostname should be something like "newName.infra.com".

Download the iRedMail installer:
```
wget -O iRedMail_1.3.2.tar.gz https://github.com/iredmail/iRedMail/archive/1.3.2.tar.gz
```

Extract the downloaded file
```shell
tar zxf iRedMail_1.3.2.tar.gz
```

Launch the installation
```
chmod +x iRedMail.sh
./iRedMail.sh
```

Then, proceed with the installation and answer to the questions you will be asked.

In our case, we chose:
- Database solution: MariaDB
- Admin panel: iRedAdmin
- WebMail: RoundCube, SOGo
- Calendar, Agenda: SOGo
- Ban system: Fail2ban

You'll also have to give passwords for the database and for the mailing system.
After that, the installation will continue and hopefully succeed.

A restart is required and you will be able to connect to your services:
- *domain/mail* for the email
- *domain/SOGo* for the calendar/agenda
- *domain/iredadmin* for the admin panel

The default admin user is **postmaster@domain**

To create a new user, you can go on *domain/iredadmin/create/user/domain*, and then fill the fields.
To disable an existing user, you have to go on *domain/iredadmin/profile/user/general/user@domain* and uncheck the case "Enable this account".


